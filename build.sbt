name := "es-master-thesis"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= {

  val akkaVersion = "2.4.8"
  val sprayVersion = "1.3.2"

  Seq(
  	"com.typesafe"				%  "config"									% "1.3.0",
	"com.typesafe.akka"			%% "akka-persistence-query-experimental"	% akkaVersion,
	"com.typesafe.akka"			%% "akka-stream"							% akkaVersion,
	"com.typesafe.akka" 		%% "akka-http-core" 						% akkaVersion,
	"com.typesafe.akka" 		%% "akka-persistence" 						% akkaVersion,
	"com.typesafe.akka" 		%% "akka-slf4j" 							% akkaVersion,
	"io.spray"					%% "spray-client"							% sprayVersion,
	"io.spray"					%% "spray-routing"							% sprayVersion,
	"io.spray"					%% "spray-json"								% sprayVersion,
	"io.spray"					%% "spray-can"								% sprayVersion,
    "ch.qos.logback" 			% "logback-classic" 						% "1.1.3",
	"org.reactivemongo" 		%% "reactivemongo" 							% "0.11.9",
	"com.github.scullxbones"    %% "akka-persistence-mongo-rxmongo" 		% "1.2.5",
	"com.typesafe.akka" 		%% "akka-testkit" 							% akkaVersion   	% "test",
	"org.scalatest" 			%% "scalatest" 								% "2.2.4"   	 	% "test",
	"io.spray"					%% "spray-testkit"							% sprayVersion		% "test",
	"org.json4s" 				%% "json4s-native" 							% "3.3.0"
  )
}