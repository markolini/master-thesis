package actors

import akka.NotUsed
import akka.actor.{ Actor, ActorLogging, Props, actorRef2Scala }
import akka.contrib.persistence.mongodb.{ MongoReadJournal, ScalaDslMongoReadJournal }
import akka.persistence.query.{ EventEnvelope, PersistenceQuery }
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import domain.Event
import protocols.AccountsProtocol.{ AccountClosed, AccountDeposit, AccountOpened, AccountStatusChanged, AccountWithdrawl }
import scala.collection.mutable.MutableList
import scala.collection.mutable.ListBuffer

object ReaderActor {

  def createReader(): Props = Props(classOf[ReaderActor]);
}

class ReaderActor extends Actor with ActorLogging {

  import protocols.ClientReaderProtocol._

  override def receive = {
    case FetchAllEvents() => {
      var events = List[Event]()

      implicit val materializer = ActorMaterializer()
      val query = PersistenceQuery(context.system).readJournalFor[ScalaDslMongoReadJournal](MongoReadJournal.Identifier)

      query.allEvents().map(_.event).runForeach {
        case AccountStatusChanged(id, status)      => events = Event("AccountStatusChanged", id, "", status) :: events
        case AccountDeposit(id, amountDeposited)   => events = Event("AccountDeposit", id, amountDeposited.toString(), "") :: events
        case AccountWithdrawl(id, amountWithdrawn) => events = Event("AccountWithdrawl", id, amountWithdrawn.toString(), "") :: events
        case AccountClosed(id, closedStatus)       => events = Event("AccountClosed", id, "", closedStatus) :: events
        case AccountOpened(id, openedStatus)       => events = Event("AccountOpened", id, "", openedStatus) :: events
      }

      Thread.sleep(3000)

      for (event <- events) {
        println("Printing fetched events" + event.toString())
      }

      sender() ! SendEvents(events);
    }
  }
}
