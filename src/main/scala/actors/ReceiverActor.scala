package actors

import akka.actor.{ Actor, ActorLogging, Props, actorRef2Scala }
import domain.Work
import protocols.ManagerReceiverProtocol.{ ClientCommand, ClientWantsToCloseAnAccount, ClientWantsToKnowTheStateOfTheAccount, ClientWantsToMakeAnDeposit, ClientWantsToOpenAnAccount, ClientWantsToUpdateAccountStatus, ClientWatsToMakeAnWithdraw, ReceiveClientCommand }
import domain.Money

object ReceiverActor {
  def createReceiver(): Props = Props(classOf[ReceiverActor]);
}

class ReceiverActor extends Actor with ActorLogging {

  import domain.Money._
  import protocols.ReceiverValidatorProtocol._

  private var availableWork = List[Work]()

  override def receive = {
    case FetchWork() => {
      if (availableWork.isEmpty) {
        sender() ! NoWorkAtTheMoment();
      } else {
        sender() ! ReceiveCommand(availableWork.head.clientCommand);
        availableWork = availableWork.tail;
      }
    }

    case ReceiveClientCommand(cmd) => {
      addToWorkList(cmd)
    }
  }

  def addToWorkList(cmd: ClientCommand): Unit = {
    cmd match {
      case ClientWantsToOpenAnAccount()                            => availableWork = new Work(Open()) :: availableWork
      case ClientWantsToCloseAnAccount(accountId)                  => availableWork = new Work(Close(accountId)) :: availableWork
      case ClientWantsToUpdateAccountStatus(accountId, status)     => availableWork = new Work(Change(accountId, status)) :: availableWork
      case ClientWantsToMakeAnDeposit(accountId, amount, currency) => availableWork = new Work(Deposit(accountId, Money.getInstance(amount, currency))) :: availableWork
      case ClientWatsToMakeAnWithdraw(accountId, amount, currency) => availableWork = new Work(Withdraw(accountId, Money.getInstance(amount, currency))) :: availableWork
      case ClientWantsToKnowTheStateOfTheAccount(accountId)        => availableWork = new Work(AccountState(accountId)) :: availableWork
    }
  }
}
