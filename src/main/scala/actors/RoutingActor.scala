package actors

import akka.actor.{ Actor, ActorLogging, ActorSelection }
import akka.actor.ActorSelection.toScala
import akka.actor.Props

object RoutingActor {
  def createRouter(): Props = Props(classOf[RoutingActor]);
}

class RoutingActor() extends Actor with ActorLogging {

  import protocols.RouterWorkerProtocol._
  import protocols.ValidatorRouterProtocol._
  import scala.collection.mutable.Map

  private var actorMap = Map[String, String]();

  override def receive = {
    case OpenMyAccount(metadata) => {
      createOrFetchExistingActor(metadata.accountId) ! OpenAccount(metadata.accountId, metadata.accountStatus);
    };

    case CloseMyAccount(metadata) => {
      createOrFetchExistingActor(metadata.accountId) ! CloseAccount(metadata.accountId, metadata.accountStatus);
    };

    case ChangeMyAccountState(metadata) => {
      createOrFetchExistingActor(metadata.accountId) ! ChangeAccountStatus(metadata.accountId, metadata.accountStatus);
    };

    case DepositToMyAccount(metadata) => {
      createOrFetchExistingActor(metadata.accountId) ! Deposit(metadata.accountId, metadata.depositAmount);
    };

    case WithdrawFromMyAccout(metadata) => {
      createOrFetchExistingActor(metadata.accountId) ! Withdraw(metadata.accountId, metadata.withdrawAmount);
    };

    case FetchMyAccountState(metadata) => {
      createOrFetchExistingActor(metadata.accountId) ! FetchState(metadata.accountId);
    };
  }

  def createWorkerActor(persistIdAddon: String): Unit = {
    actorMap.update(persistIdAddon, context.actorOf(WorkerActor.createWorker(persistIdAddon), persistIdAddon).path.toString())
  }

  def createOrFetchExistingActor(persistIdAddon: String): ActorSelection = {
    if (actorMap.contains(persistIdAddon)) {
      return context.actorSelection(actorMap.get(persistIdAddon).get)
    } else {
      createWorkerActor(persistIdAddon);
      return context.actorSelection(actorMap.get(persistIdAddon).get)
    }
  }
}
