package actors

import scala.concurrent.duration.DurationInt

import akka.actor.{ Actor, ActorLogging, ActorRef, Props, actorRef2Scala }
import protocols.ReceiverValidatorProtocol.{ Change, Close, Deposit, FetchWork, NoWorkAtTheMoment, Open, ReceiveCommand, Withdraw, AccountState }
import domain.AccountStateMetaDataObject

object ValidatorActor {
  def createValidator(routerRef: ActorRef, receiverRef: ActorRef): Props = Props(classOf[ValidatorActor], routerRef, receiverRef)
}

class ValidatorActor(routerRef: ActorRef, receiverRef: ActorRef) extends Actor with ActorLogging {

  import context.dispatcher
  import domain.{ AccountCloseMetaDataObject, AccountDepositMetaDataObject, AccountStatusChangeMetaDataObject, AccountWithdrawMetaDataObject, Money, TransactionMetaData }
  import protocols.ValidatorRouterProtocol._
  import scala.concurrent.duration._

  context.system.scheduler.schedule(5.seconds, 5.seconds, receiverRef, FetchWork())

  override def receive = {
    case ReceiveCommand(cmd) => {
      log.info("Received work. Validating command")
      cmd match {
        case Open()                        => sendOpenCommand();
        case Close(accountId)              => sendCloseCommand(accountId);
        case Change(accountId, status)     => sendChangeCommand(accountId, status);
        case Deposit(accountId, deposit)   => sendDepositCommand(accountId, deposit);
        case Withdraw(accountId, withdraw) => sendWithdrawCommand(accountId, withdraw);
        case AccountState(accountId)       => sendAccountStateCommand(accountId);
      }
    };

    case NoWorkAtTheMoment() => {
      //      log.info("No work available. Will try again later");
    }
  }

  def sendOpenCommand(): Unit = {
    log.info("Received OPEN_ACCOUNT command. Sending to router")
    routerRef ! OpenMyAccount(TransactionMetaData.createAccountOpenMetaDataObject);
  }

  def sendCloseCommand(accountId: String): Unit = {
    log.info("Received CLOSE_ACCOUNT command. Sending to router")
    routerRef ! CloseMyAccount(new AccountCloseMetaDataObject(accountId));
    fetchMoreWork();
  }

  def sendChangeCommand(accountId: String, status: String): Unit = {
    log.info("Received CHANGE_STATUS command. Sending to router")
    routerRef ! ChangeMyAccountState(new AccountStatusChangeMetaDataObject(accountId, status))
    fetchMoreWork();
  }

  def sendDepositCommand(accountId: String, deposit: Money): Unit = {
    log.info("Received DEPOSIT command. Sending to router")
    routerRef ! DepositToMyAccount(new AccountDepositMetaDataObject(accountId, deposit))
    fetchMoreWork();
  }

  def sendWithdrawCommand(accountId: String, withdraw: Money): Unit = {
    log.info("Received WITHDRAW command. Sending to router")
    routerRef ! WithdrawFromMyAccout(new AccountWithdrawMetaDataObject(accountId, withdraw))
    fetchMoreWork();
  }

  def sendAccountStateCommand(accountId: String): Unit = {
    log.info("Received ACCOUNT_STATE command. Sending to router")
    routerRef ! FetchMyAccountState(new AccountStateMetaDataObject(accountId))
    fetchMoreWork();
  }

  def fetchMoreWork(): Unit = {
    receiverRef ! FetchWork();
  }
}
