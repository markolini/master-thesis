package actors

import akka.actor.{ ActorLogging, Props }
import akka.persistence.{ PersistentActor, RecoveryCompleted, SnapshotOffer }
import protocols.AccountsProtocol

object WorkerActor {
  def createWorker(actorPath: String): Props = Props(classOf[WorkerActor], actorPath);
}

class WorkerActor(persistenceIdAddon: String) extends PersistentActor with ActorLogging {

  import AccountsProtocol._
  import protocols.RouterWorkerProtocol._

  def persistenceId: String = "Worker" + persistenceIdAddon;

  var state: AccountState = AccountsProtocol.createTempDummyAccount()

  def updateState(evt: AccountEvent): Unit = {
    state = state.updated(evt);
  }

  def receiveCommand: Receive = {
    case OpenAccount(id, status) => {
      persist(AccountOpened(id, status))(evt => {
        log.info(s"Account opened with ID ${id}")
        updateState(evt);
      })
    };

    case CloseAccount(id, status) => {
      persist(AccountClosed(id, status))(evt => {
        log.info(s"Account closed with ID ${id}")
        updateState(evt);
      })
    };

    case ChangeAccountStatus(id, status) => {
      persist(AccountStatusChanged(id, status))(evt => {
        log.info(s"updated with status ${status} on account with ID ${id}")
        updateState(evt);
      })
    };

    case Deposit(id, amount) => {
      persist(AccountDeposit(id, amount))(evt => {
        log.info(s"Amount ${amount} deposited to account number ${id}}")
        updateState(evt);
      })
    };

    case Withdraw(id, amount) => {
      persist(AccountWithdrawl(id, amount))(evt => {
        log.info(s"Amount ${amount} withrawed to account number ${id}}")
        updateState(evt);
      })
    };

    case FetchState(accountId) =>
      println(s"The Current state of the account ${accountId} is ${state}")
  };

  def receiveRecover: Receive = {
    case evt: AccountEvent => {
      println(s"Current state of the Account on recovery mode from journal: ${evt} ")
      updateState(evt)
    }

    case SnapshotOffer(_, snapshot: AccountState) => {
      println(s"Current state of the Account on recovery mode from snapshot: ${snapshot} ")
      state = snapshot
    }

    case RecoveryCompleted => {
      println(s"Recovery completed. Switching to receiving mode")
    }
  }
}
