package adapters

import akka.persistence.journal.{ EventAdapter, EventSeq }
import domain.Event
import protocols.AccountsProtocol.{ AccountClosed, AccountDeposit, AccountOpened, AccountStatusChanged, AccountWithdrawl }

class JournalEventAdapter extends EventAdapter {

  override def manifest(event: Any): String = ""

  override def fromJournal(event: Any, manifest: String): EventSeq = event match {
    case AccountStatusChanged(id, updatedStatus) => EventSeq(Event(event.getClass().getCanonicalName.substring(27), id, "", updatedStatus));
    case AccountDeposit(id, amountDeposited)     => EventSeq(Event(event.getClass().getCanonicalName.substring(27), id, amountDeposited.toString(), ""));
    case AccountWithdrawl(id, amountWithdrawn)   => EventSeq(Event(event.getClass().getCanonicalName.substring(27), id, amountWithdrawn.toString(), ""));
    case AccountClosed(id, closedStatus)         => EventSeq(Event(event.getClass().getCanonicalName.substring(27), id, "", closedStatus));
    case AccountOpened(id, openedStatus)         => EventSeq(Event(event.getClass().getCanonicalName.substring(27), id, "", openedStatus));
  }

  override def toJournal(event: Any): Any = event
}
