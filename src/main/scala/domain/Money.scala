package domain

import java.io.Serializable
import java.math.BigDecimal
import java.util.Currency

object Money {
  def getInstance(amount: String, currencyCode: String): Money = {
    new Money(new BigDecimal(amount), Currency.getInstance(currencyCode))
  }
}

class Money(val amount: BigDecimal, val currency: Currency) extends Serializable {

  override def toString(): String = {
    return amount.toString() + " " + currency.toString();
  }
}
