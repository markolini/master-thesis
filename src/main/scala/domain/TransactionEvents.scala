package domain

import scala.collection.mutable.ListBuffer

import spray.json._
import DefaultJsonProtocol._

import scala.collection.mutable.ListBuffer

case class Event(name: String, accountId: String, amount: String, status: String)

object Event extends DefaultJsonProtocol {
  implicit val appointmentFormat = jsonFormat4(Event.apply)
}
