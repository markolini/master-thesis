package domain

abstract class AccountMetaData(val accountId: String)

object TransactionMetaData {

  def createAccountOpenMetaDataObject(): AccountOpenMetaDataObject = {
    return new AccountOpenMetaDataObject(generateAccountNumber);
  }

  def generateAccountNumber(): String = {
    return scala.util.Random.nextInt(10000).toString();
  }
}

class AccountOpenMetaDataObject(override val accountId: String, val accountStatus: String) extends AccountMetaData(accountId) {
  def this(accountId: String) {
    this(accountId, "STATUS_OPENED")
  }
}

class AccountCloseMetaDataObject(override val accountId: String, val accountStatus: String) extends AccountMetaData(accountId) {
  def this(accountId: String) {
    this(accountId, "STATUS_CLOSED")
  }
}

class AccountStatusChangeMetaDataObject(override val accountId: String, val accountStatus: String) extends AccountMetaData(accountId)
class AccountDepositMetaDataObject(override val accountId: String, val depositAmount: Money) extends AccountMetaData(accountId)
class AccountWithdrawMetaDataObject(override val accountId: String, val withdrawAmount: Money) extends AccountMetaData(accountId)
class AccountStateMetaDataObject(override val accountId: String) extends AccountMetaData(accountId)
