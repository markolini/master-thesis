package entrypoint

import rest.{ Actors, RestInterface }

object TransactionSystem extends App with RestInterface with Actors
