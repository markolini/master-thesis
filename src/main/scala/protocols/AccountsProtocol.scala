package protocols

import domain.Money

object AccountsProtocol {

  sealed trait AccountEvent;

  // account status change
  case class AccountStatusChanged(accountId: String, status: String) extends AccountEvent;

  // account transactions
  case class AccountDeposit(accountId: String, amount: Money) extends AccountEvent;
  case class AccountWithdrawl(accountId: String, amount: Money) extends AccountEvent;

  // account actions
  case class AccountClosed(accountId: String, status: String) extends AccountEvent;
  case class AccountOpened(accountId: String, status: String) extends AccountEvent;

  case class AccountState(accountId: String, amount: Money, status: String) {

    def updated(evt: AccountEvent): AccountState = evt match {
      case AccountStatusChanged(id, newStatus)  => copy(accountId = id, amount, status = newStatus)
      case AccountDeposit(id, toBeAdded)        => copy(accountId = id, add(amount, toBeAdded), status)
      case AccountWithdrawl(id, toBeSubtracted) => copy(accountId = id, subtract(amount, toBeSubtracted), status)
      case AccountClosed(id, newStatus)         => copy(accountId = id, amount, status = newStatus)
      case AccountOpened(id, newStatus)         => copy(accountId = id, amount, status = newStatus)
      case _                                    => this
    };

    def add(currentAmount: Money, amountToAdd: Money): Money = {
      val newAmount = currentAmount.amount.add(amountToAdd.amount);
      return Money.getInstance(newAmount.toString(), currentAmount.currency.getCurrencyCode);
    }

    def subtract(currentAmount: Money, amountToRemove: Money): Money = {
      val newAmount = currentAmount.amount.subtract(amountToRemove.amount);
      return Money.getInstance(newAmount.toString(), currentAmount.currency.getCurrencyCode);
    }
  }

  def createTempDummyAccount(): AccountState = {
    AccountState("000000000", Money.getInstance("0", "HRK"), "temporary");
  }
}
