package protocols

import domain.Event

object ClientReaderProtocol {

  sealed trait ClientReadCommand
  case class FetchAllEvents() extends ClientReadCommand

  case class SendEvents(events: List[Event])
}
