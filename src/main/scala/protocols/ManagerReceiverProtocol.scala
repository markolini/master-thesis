package protocols

object ManagerReceiverProtocol {

  sealed trait ClientCommand
  case class ClientWantsToOpenAnAccount() extends ClientCommand
  case class ClientWantsToCloseAnAccount(accountId: String) extends ClientCommand
  case class ClientWantsToUpdateAccountStatus(accountId: String, status: String) extends ClientCommand
  case class ClientWantsToMakeAnDeposit(accountId: String, amount: String, currency: String) extends ClientCommand
  case class ClientWatsToMakeAnWithdraw(accountId: String, amount: String, currency: String) extends ClientCommand
  case class ClientWantsToKnowTheStateOfTheAccount(accountId: String) extends ClientCommand

  case class ReceiveClientCommand(clientCmd: ClientCommand);
}
