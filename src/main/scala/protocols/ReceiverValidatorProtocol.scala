package protocols

import domain.Money

object ReceiverValidatorProtocol {

  sealed trait ReceiverCommand;
  case class Open() extends ReceiverCommand;
  case class Close(accountId: String) extends ReceiverCommand;
  case class Change(accountId: String, status: String) extends ReceiverCommand;
  case class Deposit(accountId: String, deposit: Money) extends ReceiverCommand;
  case class Withdraw(accountId: String, withdraw: Money) extends ReceiverCommand;
  case class AccountState(accountId: String) extends ReceiverCommand;

  case class ReceiveCommand(cmd: ReceiverCommand);
  case class FetchWork();
  case class NoWorkAtTheMoment();
}
