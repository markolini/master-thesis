package protocols

import domain.Money

object RouterWorkerProtocol {

  sealed trait Command
  case class OpenAccount(accountId: String, status: String) extends Command
  case class CloseAccount(accountId: String, status: String) extends Command
  case class ChangeAccountStatus(accountId: String, status: String) extends Command
  case class Deposit(accountId: String, amount: Money) extends Command
  case class Withdraw(accountId: String, amount: Money) extends Command
  case class FetchState(accountId: String) extends Command;
}
