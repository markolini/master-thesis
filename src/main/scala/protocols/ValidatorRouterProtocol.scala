package protocols

import domain.{ AccountCloseMetaDataObject, AccountDepositMetaDataObject, AccountOpenMetaDataObject, AccountStateMetaDataObject, AccountStatusChangeMetaDataObject, AccountWithdrawMetaDataObject }

object ValidatorRouterProtocol {

  sealed trait ValidatorCommands
  case class OpenMyAccount(metadata: AccountOpenMetaDataObject) extends ValidatorCommands
  case class CloseMyAccount(metadata: AccountCloseMetaDataObject) extends ValidatorCommands
  case class ChangeMyAccountState(metadata: AccountStatusChangeMetaDataObject) extends ValidatorCommands
  case class DepositToMyAccount(metadata: AccountDepositMetaDataObject) extends ValidatorCommands
  case class WithdrawFromMyAccout(metadata: AccountWithdrawMetaDataObject) extends ValidatorCommands
  case class FetchMyAccountState(metadata: AccountStateMetaDataObject) extends ValidatorCommands
}
