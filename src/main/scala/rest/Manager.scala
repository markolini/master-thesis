package rest

import akka.actor.{ Actor, ActorRef, Props, actorRef2Scala }
import domain.Event
import protocols.ClientReaderProtocol.{ FetchAllEvents, SendEvents }
import protocols.ManagerReceiverProtocol.{ ClientWantsToCloseAnAccount, ClientWantsToKnowTheStateOfTheAccount, ClientWantsToMakeAnDeposit, ClientWantsToOpenAnAccount, ClientWantsToUpdateAccountStatus, ClientWatsToMakeAnWithdraw, ReceiveClientCommand }
import spray.httpx.SprayJsonSupport.sprayJsonMarshaller
import spray.httpx.marshalling.ToResponseMarshallable.isMarshallable
import spray.routing.Directive.pimpApply
import spray.routing.SimpleRoutingApp
import spray.routing.directives.ParamDefMagnet.apply

object RestActor {

  def createRest(receiverActor: ActorRef, readerActor: ActorRef): Props = Props(classOf[RestActor], receiverActor, readerActor);
}

class RestActor(val receiverActor: ActorRef, val readerActor: ActorRef) extends Actor with SimpleRoutingApp {

  implicit val system = context.system
  private var events = List[Event]();

  override def preStart() = {
    createRestRoutesAndStartServer();
    readerActor ! FetchAllEvents();
  }

  override def receive = {
    case SendEvents(response) => {

      for (event <- response) {
        println("Printing response" + event.toString())
      }

      events = response

      for (event <- events) {
        println("Printing added events" + event.toString())
      }
    }
  }

  def createRestRoutesAndStartServer(): Unit = {

    val index = get {
      path("") {
        complete {
          "Backend application started. Ready for service calls.";
        }
      }
    }

    val accountOpenRoute = get {
      path("open" / "account") {
        complete {
          receiverActor ! ReceiveClientCommand(ClientWantsToOpenAnAccount());
          "Account opened";
        }
      }
    }

    val accountCloseRoute = get {
      path("close" / "account") {
        parameters("accountId".as[String]) { (accountId) =>
          receiverActor ! ReceiveClientCommand(ClientWantsToCloseAnAccount(accountId));
          complete {
            s"Account ${accountId} closed";
          }
        }
      }
    }

    val accountStateChangeRoute = get {
      path("change" / "status") {
        parameters("accountId".as[String], "status".as[String]) { (accountId, status) =>
          receiverActor ! ReceiveClientCommand(ClientWantsToUpdateAccountStatus(accountId, status));
          complete {
            s"Account ${accountId} status changed to: ${status}}";
          }
        }
      }
    }

    val accountDepositRoute = get {
      path("account" / "deposit") {
        parameters("accountId".as[String], "depositAmount".as[String], "currencyCode".as[String]) { (accountId, depositAmount, currencyCode) =>
          receiverActor ! ReceiveClientCommand(ClientWantsToMakeAnDeposit(accountId, depositAmount, currencyCode));
          complete {
            s"Amount ${depositAmount} ${currencyCode} deposited to account number ${accountId}}";
          }
        }
      }
    }

    val accountWithdrawRoute = get {
      path("account" / "withdraw") {
        parameters("accountId".as[String], "withdrawAmount".as[String], "currencyCode".as[String]) { (accountId, withdrawAmount, currencyCode) =>
          receiverActor ! ReceiveClientCommand(ClientWatsToMakeAnWithdraw(accountId, withdrawAmount, currencyCode));
          complete {
            s"Amount ${withdrawAmount} ${currencyCode} withdrawed to account number ${accountId}}";
          }
        }
      }
    }

    val fetchAccountEvents = get {
      path("fetch" / "account" / "events") {
        complete {
          readerActor ! FetchAllEvents();

          events
        }
      }
    }

    val accountState = get {
      path("account" / "state") {
        parameters("accountId".as[String]) { (accountId) =>
          receiverActor ! ReceiveClientCommand(ClientWantsToKnowTheStateOfTheAccount(accountId));
          complete {
            s"Fetching account state for account ID ${accountId}";
          }
        }
      }
    }

    //Starting REST interface server
    startServer(interface = "localhost", port = 8080) {
      index ~
        accountOpenRoute ~
        accountCloseRoute ~
        accountStateChangeRoute ~
        accountDepositRoute ~
        accountWithdrawRoute ~
        fetchAccountEvents ~
        accountState
    }
  }
}
