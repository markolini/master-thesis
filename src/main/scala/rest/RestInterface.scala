package rest

import actors.{ ReaderActor, ReceiverActor, RoutingActor, ValidatorActor }
import akka.actor.ActorSystem

trait Core {

  protected implicit def system: ActorSystem
}

trait RestInterface extends Core {
  val system = ActorSystem("ActorSystem");
}

trait Actors {
  this: Core =>
  val receiver = system.actorOf(ReceiverActor.createReceiver(), "Receiver")
  val router = system.actorOf(RoutingActor.createRouter(), "Router")
  val validator = system.actorOf(ValidatorActor.createValidator(router, receiver), "Validator")
  val reader = system.actorOf(ReaderActor.createReader(), "Reader")
  val rest = system.actorOf(RestActor.createRest(receiver, reader), "Rest")
}
